const webpack = require('webpack')
    , path = require('path')

module.exports = {
    entry: './app/ts/app.ts',
    output: {
        path: path.resolve(__dirname + '/app/dist', 'js'),
        filename: 'app.bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [ 
            { test: /\.tsx?$/, loader: 'ts-loader' }
        ]
    }
}