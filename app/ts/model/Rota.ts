export class Rota {

	private _titulo : string

	constructor(titulo: string) {

		this._titulo = titulo

	}

	get url() : string {

		let url = this._titulo

		url = this._removeAcento(url)
		url = this._trocaEspaco(url,'-')
		url = url.toLowerCase()

		return `/${url}`

	}

	private _trocaEspaco(texto: string, simbolo: string) : string {

		return texto.replace(new RegExp(/\s/g),simbolo)

	}

	private _removeAcento(texto: string) : string {

		let r = texto.replace(new RegExp(/[àáâãäå]/g),'a')
		r = r.replace(new RegExp(/æ/g),'ae')
		r = r.replace(new RegExp(/ç/g),'c')
		r = r.replace(new RegExp(/[èéêë]/g),'e')
		r = r.replace(new RegExp(/[ìíîï]/g),'i')
		r = r.replace(new RegExp(/ñ/g),'n')                
		r = r.replace(new RegExp(/[òóôõö]/g),'o')
		r = r.replace(new RegExp(/œ/g),'oe')
		r = r.replace(new RegExp(/[ùúûü]/g),'u')
		r = r.replace(new RegExp(/[ýÿ]/g),'y')
		return r
		
	}

}