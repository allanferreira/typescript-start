import {Rota} from '../model/Rota'
export class RotaController {
    
    private _titulo : string

    constructor(titulo: Element){

        this._titulo = titulo.innerHTML

    }

    criaRota() : void {
        
        let rota = new Rota(this._titulo)
        document
            .querySelector('span')
            .innerHTML = rota.url

    }

}